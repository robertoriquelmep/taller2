package root.services;

import java.io.IOException;
import java.time.LocalDate;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.Response;

public class ObtieneDatos {
    
   @GET
   @Path("/dolar/{fecha}")
   public String obtieneValorDolarPorFecha(@PathParam("fecha") String stringFecha) throws WebApplicationException, IOException{
       Client cliente = ClientBuilder.newClient();
       LocalDate fechaActual = java.time.LocalDate.now();
       LocalDate today = LocalDate.now();
       int year = today.getYear();
       int month = today.getMonthValue();
       int day = today.getDayOfMonth();
       String textFallo;
       String observacion = null;
       String url = "https://api.sbif.cl/api-sbifv3/recursos_api/dolar/"+year+"*/"+month+"/dias/"+day+"?formato=JSON&apikey="+token();
       Response respuestaAPI = cliente.target(url).request().get();
       String StringUserDate = "";
       String responseData = respuestaAPI.readEntity(String.class);
       LocalDate fechaUsuario = LocalDate.parse(stringFecha);
       LocalDate sysDate = java.time.LocalDate.now();
       if (fechaUsuario.compareTo(sysDate) < 1) {
           while (responseData.contains("404")){
               textFallo = "{'observacion':**AVISO**: 'No existe informaci�n para la fecha consultada. Valor m�s cercano a continuaci�n'}";
               fechaUsuario = fechaUsuario.minusDays(1);
               url = "https://api.sbif.cl/api-sbifv3/recursos_api/dolar/"+year+"*/"+month+"/dias/"+day+"?formato=JSON&apikey="+token();
               respuestaAPI = cliente.target(url).request().get();
               responseData = respuestaAPI.readEntity(String.class);
           }
       }else{
           textFallo = "{'observacion': 'Fecha invalida. Se muestra fecha actual o mas cercana'}";
           while (responseData.contains("404")){
               fechaUsuario = fechaUsuario.minusDays(1);
               url = "https://api.sbif.cl/api-sbifv3/recursos_api/dolar/"+year+"*/"+month+"/dias/"+day+"?formato=JSON&apikey="+token();
               respuestaAPI = cliente.target(url).request().get();
               responseData = respuestaAPI.readEntity(String.class);
           }
       }
        return observacion + responseData;
   }
   
   private String token(){
       String token = "c7b86aeebc9bfdfc692a9f922c9445562352d436";
       return token;
   }
}